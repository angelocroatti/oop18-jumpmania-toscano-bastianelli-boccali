package controller.menu;

import common.MsgStrings;
import enumerators.SceneType;
import view.BackgroundMusic;
import view.GenericView;
import view.menu.EndGameView;

/**
 * EndGame controller. It extends Controller interface and is register in the
 * EventBus. Create and handle the EndGameView
 */
public class EndGameMenu extends AbstractMenuController {

    private final EndGameView view;

    /**
     * EndGame constructor.
     */
    public EndGameMenu() {
        super();
        view = new EndGameView(this);
        this.view.setInfo(getModel().getUser().getCoin(), getModel().getUser().getMaxHeight());
    }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.MENU:
            this.postSceneAndUnregister(SceneType.MENU);
            break;
        case MsgStrings.RESTART:
            this.postSceneAndUnregister(SceneType.NEW_GAME);
            break;
        case MsgStrings.SWITCH_AUDIO_MODE:
            BackgroundMusic.getInstance().switchAudioMode();
            break;
        default:
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }
}
