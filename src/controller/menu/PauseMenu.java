package controller.menu;

import common.MsgStrings;
import controller.GameControllerImpl;
import enumerators.SceneType;
import view.BackgroundMusic;
import view.GenericView;
import view.menu.PauseView;

/**
 * The pause menu controller. This class create and handle the PauseView.
 */
public class PauseMenu extends AbstractMenuController {

    private final PauseView view;

    /**
     * PauseMenu constructor.
     */
    public PauseMenu() {
        super();
        view = new PauseView(this);
        this.view.setInfo(getModel().getUser().getCoin(), getModel().getUser().getMaxHeight());
   }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.RESUME:
            this.postSceneAndUnregister(SceneType.RESUME_GAME);
            break;
        case MsgStrings.SWITCH_AUDIO_MODE:
            BackgroundMusic.getInstance().switchAudioMode();
            break;
        case MsgStrings.MENU:
            GameControllerImpl.getInstance().gameOver();
            this.postSceneAndUnregister(SceneType.MENU);
            break;
        default:
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

}
