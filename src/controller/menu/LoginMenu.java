package controller.menu;

import common.MsgStrings;
import enumerators.SceneType;
import model.user.LoginModel;
import view.GenericView;
import view.menu.LoginView;

/**
 * The login menu controller class. This class create the LoginView and manage
 * MsgEvent.
 */
public class LoginMenu extends AbstractMenuController {

    private final LoginView view;
    private LoginModel log;

    /**
     * LoginMenu constructor. Create the LoginView view
     */
    public LoginMenu() {
        super();
        this.view = new LoginView(this);
    }

    @Override
    public final void sendMsg(final String msg) {
        switch (msg) {
        case MsgStrings.LOGIN:
            if (login()) {
                this.postSceneAndUnregister(SceneType.MENU);
            }
            break;
        case MsgStrings.REGISTER:
            if (newProfile()) {
                this.postSceneAndUnregister(SceneType.MENU);
            }
            break;
        default:
            break;
        }
    }

    @Override
    public final GenericView getView() {
        return view;
    }

    /**
     * Create a UserProfile with the credential given by input. The password is
     * crypted.
     * 
     * @return The UserProfile created.
     */
    private void getInputFromView() {
        log = new LoginModel(view.getUsername(), view.getPassword());
    }

    /**
     * Check if fields in LoginView are filled.
     * 
     * @return true if username and password are present
     */
    private boolean isInputPresent() {
        return ((view.getUsername().isEmpty() || view.getPassword().isEmpty()) ? false : true);
    }

    private boolean login() {
        if (isInputPresent()) {
            getInputFromView();
            if (log.loginAndSetUser()) {
                return true;
            } else {
                setErrorView();
            }
        } else {
            view.setErrorMsg("Username and Password can't be empty");
        }
        return false;
    }

    private boolean newProfile() {
        if (isInputPresent()) {
            getInputFromView();
           if (log.registerAndSetUser()) {
               return true;
           } else {
               setErrorView();
           }
        } else {
            view.setErrorMsg("Username and Password can't be empty");
        }
        return false;
    }

    private void setErrorView() {
        log.getErrorMsg().ifPresent(e -> view.setErrorMsg(e));
    }
}
