package common;

/**
 * Class that contains constant message strings.
 */
public final class MsgStrings {

    /**
     * Login menu "login" string.
     */
    public static final String LOGIN = "Login";
    /**
     * Pause menu "pause" string.
     */
    public static final String PAUSE = "Pause";
    /**
     * Menu "level" string.
     */
    public static final String LEVEL = "Level";
    /**
     * Login menu "register" string.
     */
    public static final String REGISTER = "Register";
    /**
     * "switch audio" string.
     */
    public static final String SWITCH_AUDIO_MODE = "Audio";
    /**
     * Menu "statistics" string.
     */
    public static final String STATISTICS = "Profile";
    /**
     * End game menu "menu" string.
     */
    public static final String MENU = "Menu";
    /**
     * Pause menu "resume" string.
     */
    public static final String RESUME = "Resume";
    /**
     * Shop menu "buy" string.
     */
    public static final String BUY_CHARACTER = "Buy";
    /**
     * Main menu "shop" string.
     */
    public static final String SHOP = "Shop";
    /**
     * End game menu "end game" string.
     */
    public static final String END_GAME = "End Game";
    /**
     * End game menu "restart" string.
     */
    public static final String RESTART = "Restart";
    /**
     * Shop menu "set character" string.
     */
    public static final String SET_CHARACTER = "Set";
    /**
     * Win menu "next level" string.
     */
    public static final String NEXT_LEVEL = "Next level";
    /**
     * Win menu "next level" string.
     */
    public static final String WIN = "Level win";

    private MsgStrings() { }
}
