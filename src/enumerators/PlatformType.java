package enumerators;

/**
 * Contain all the platform those can be found in the game. A platform type has
 * to contain at least its width and height
 */
public enum PlatformType implements SpecificType {
    /**
     * Simple platform.
     */
    SIMPLE("platf.png", 70, 10), 
    /**
     * Superjump platform, makes the player jump higher.
     */
    SUPERJUMP("superplatf.png", 70, 10), 
    /**
     * Onejump platform, disappears after a jump.
     */
    ONEJUMP("oneplatf.png", 70, 10);

    private int width;
    private int height;
    private String imageName;

    PlatformType(final String imageName, final int width, final int height) {
        this.imageName = imageName;
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the width of the platform.
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the height of the platform.
     * @return height
     */
    public int getHeight() {
        return height;
    }

    @Override
    public String getImageName() {
        return imageName;
    }
}
