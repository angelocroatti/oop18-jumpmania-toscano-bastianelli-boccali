package enumerators;

import common.CommonStrings;

/**
 * File audio enumeration.
 */
public enum FileAudio {

    /**
     * MP3 Track.
     */
    TRACK_1("Fireflies.mp3");

    private String fileName;

    /**
     * File Audio constructor from file name.
     * @param fileName : the file name
     */
    FileAudio(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * Get file name.
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Get file path.
     * @return the file path
     */
    public String getPath() {
        return "music" + CommonStrings.FILE_SEPARATOR + fileName;
    }

}
