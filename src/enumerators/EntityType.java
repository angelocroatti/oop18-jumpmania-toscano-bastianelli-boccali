package enumerators;

/**
 * Enumerator for all the entity types.
 */
public enum EntityType {
    /**
     * The player entityType.
     */
    PLAYER, 
    /**
     * The enemy entityType.
     */
    ENEMY, 
    /**
     * The coin entityType.
     */
    COIN, 
    /**
     * The key entityType.
     */
    KEY, 
    /**
     * The platform entityType.
     */
    PLATFORM;
}
