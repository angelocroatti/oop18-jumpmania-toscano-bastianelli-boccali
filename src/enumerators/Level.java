package enumerators;

/**
 * Level enumertion.
 */
public enum Level {
    /**
     * Level one data.
     */
    LEVEL_1(1.0, "fire.jpg", EnemyCharacter.PARABEETLE, CoinType.SIMPLE),
    /**
     * Level two data.
     */
    LEVEL_2(1.2, "sky.jpg",  EnemyCharacter.GOOMBA, CoinType.SIMPLE),
    /**
     * Level three data.
     */
    LEVEL_3(1.3, "space.jpg", EnemyCharacter.BOMB, CoinType.SIMPLE),
    /**
     * Level four data.
     */
    LEVEL_4(1.4, "snow.jpg", EnemyCharacter.FROSTY, CoinType.SIMPLE);

    private final double difficulty;
    private final String backgroundPath;
    private final EnemyCharacter enemyCharacter;
    private final CoinType coinType;

    Level(final double difficulty, final String backgroundPath,
            final EnemyCharacter enemyCharacter, final CoinType coinType) {
        this.difficulty = difficulty;
        this.backgroundPath = backgroundPath;
        this.enemyCharacter = enemyCharacter;
        this.coinType = coinType;
    }

    /**
     * Returns the level difficulty.
     * @return difficulty factor.
     */
    public double getDifficulty() {
        return difficulty;
    }

    /**
     * Returns the path to the background image.
     * @return background image path
     */
    public String getBackgroundPath() {
        return "images/" + backgroundPath;
    }

    /**
     * Returns the enemyCharacter for the level.
     * @return enemyCharacter
     */
    public EnemyCharacter getEnemyCharacter() {
        return enemyCharacter;
    }

    /**
     * Returns the coin type of the level.
     * @return cointype
     */
    public CoinType getCoinType() {
        return coinType;
    }
}
