package model.components;

/**
 * Base component interface. Can be added into an entity to give it a new
 * behavior.
 */
public interface Component {

}
