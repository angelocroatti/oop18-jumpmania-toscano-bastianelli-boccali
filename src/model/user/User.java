package model.user;

import java.util.List;

/**
 * User interface.
 */
public interface User {

    /**
     * Get the UserProfile of the user.
     * @return the userProfile
     */
    UserProfile getUserProfile();

    /**
     * Set the UserProfile of the user.
     * @param userProfile : the userProfile
     */
    void setUserProfile(UserProfile userProfile);

    /**
     * Return a list with info and descriprion about data and profile.
     * 
     * @return a list of String
     */
    List<String> getPublicInfo();

}
