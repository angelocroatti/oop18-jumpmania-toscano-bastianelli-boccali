package model.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import enumerators.Level;
import enumerators.PlayerCharacter;

/**
 * Represent che User logic. It is composed of a UserProfile and user data.
 * This class use Flyweight pattern.
 */
public final class UserImpl implements Serializable, UserData, User {

    private static final transient long serialVersionUID = 1189272148450093523L;

    private static final String COIN = "Coin: ";
    private static final String MAX_HEIGHT = "Max Height: ";
    private static final String LEVELS = "Max level unlocked: ";
    private static final String CHARACTERS = "Characters unlocked: ";
    private static final String CURRENT_CHARACTERS = "Current character: ";

    private Level level;
    private int maxHeight;
    private int coin;
    private Set<PlayerCharacter> characters;
    private PlayerCharacter currentCharacter;

    private UserProfile userProfile;

    /**
     * Create a new User with the given profile and standard values.
     * @param userProfile : the userProfile to set
     */
    public UserImpl(final UserProfile userProfile) {
        this.setUserProfile(userProfile);
        this.level = Level.LEVEL_1;
        this.maxHeight = 0;
        this.coin = 0;
        this.characters = new HashSet<PlayerCharacter>();
        characters.add(PlayerCharacter.values()[0]);
        currentCharacter = PlayerCharacter.values()[0];
    }

    @Override
    public Level getMaxLevel() {
        return level;
    }

    @Override
    public void setMaxLevel(final Level level) {
        this.level = level;
    }

    @Override
    public int getMaxHeight() {
        return maxHeight;
    }

    @Override
    public void setMaxHeight(final int maxHeight) {
        this.maxHeight = Math.max(this.maxHeight, maxHeight);
    }

    @Override
    public HashSet<PlayerCharacter> getCharacters() {
        return new HashSet<>(characters);
    }

    @Override
    public PlayerCharacter getCurrentCharacter() {
        return currentCharacter;
    }

    @Override
    public void setCurrentCharacter(final PlayerCharacter currentCharacter) {
        this.currentCharacter = currentCharacter;
    }

    @Override
    public int getCoin() {
        return coin;
    }

    @Override
    public void setCoin(final int coin) {
        this.coin = coin;
    }

    @Override
    public void subtractCoin(final int coin) {
        if (coin >= 0 && coin <= this.coin) {
            this.coin -= coin;
        }
    }

    @Override
    public void addCoin(final int coin) {
        if (coin >= 0) {
            this.coin += coin;
        }
    }

    @Override
    public void setCharacters(final Set<PlayerCharacter> character) {
        this.characters = new HashSet<>(character);
    }

    @Override
    public void addCharacter(final PlayerCharacter character) {
        this.characters.add(character);
    }

    @Override
    public void deleteCharacter(final PlayerCharacter character) {
        this.characters.remove(character);
    }

    @Override
    public List<String> getDataList() {
        final List<String> list = new ArrayList<>();
        list.add(COIN + coin);
        list.add(MAX_HEIGHT + maxHeight);
        list.add(LEVELS + level);
        list.add(CHARACTERS);
        characters.stream().forEach(e -> list.add(e.toString()));
        list.add(CURRENT_CHARACTERS + currentCharacter.name());
        return list;
    }

    @Override
    public void unlockNewLevel(final Level currentLevel) {
        final int unlockedLevel = currentLevel.ordinal();
        try {
            setMaxLevel(Level.values()[unlockedLevel + 1]);
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Max game Level already unlocked");
        }
    }

    @Override
    public UserProfile getUserProfile() {
        return userProfile;
    }

    @Override
    public void setUserProfile(final UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public String toString() {
        return userProfile.toString() + "level=" + level + ", MaxHeight=" + maxHeight + ", coin=" + coin + ", characters=" + characters;
    }

    @Override
    public List<String> getPublicInfo() {
        final List<String> list = new ArrayList<>();
        list.add(userProfile.usernameToString());
        list.addAll(this.getDataList());
        return list;
    }
}
