package model.user;

import java.util.List;
import java.util.Set;

import enumerators.Level;
import enumerators.PlayerCharacter;

/**
 * Interface to access to User persistent data.
 */
public interface UserData {

    /**
     * @return number of level unlocked by the user
     */
    Level getMaxLevel();

    /**
     * Change current level to next level if possible. Is not possible when all
     * levels are unlocked
     * @param currentLevel TODO
     */
    void unlockNewLevel(Level currentLevel);

    /**
     * Set max level unlocked.
     * 
     * @param level : Max level unlocked
     */
    void setMaxLevel(Level level);

    /**
     * @return max height reached by the user
     */
    int getMaxHeight();

    /**
     * Set user max height reached.
     * 
     * @param maxHeight : max height reached
     */
    void setMaxHeight(int maxHeight);

    /**
     * Get the unlocked characters.
     * 
     * @return an HashSet of characters
     */
    Set<PlayerCharacter> getCharacters();

    /**
     * Get the current character selected.
     * 
     * @return the selected character
     */
    PlayerCharacter getCurrentCharacter();

    /**
     * Set the selected character as current character.
     * 
     * @param currentCharacter : the character selected
     */
    void setCurrentCharacter(PlayerCharacter currentCharacter);

    /**
     * Get the coin owned by the user.
     * 
     * @return coin owned
     */
    int getCoin();

    /**
     * Set coin value.
     * 
     * @param coin : the new coin value, must be not negative
     */
    void setCoin(int coin);

    /**
     * Add coin to the actual coin owned.
     * 
     * @param coin : the new coin value, must be not negative
     */
    void addCoin(int coin);

    /**
     * Subtrat coin to the actual coin owned.
     * 
     * @param coin : the new coin value, must be not negative
     */
    void subtractCoin(int coin);

    /**
     * Set PlayerCharacters.
     * 
     * @param character : Set of characters
     */
    void setCharacters(Set<PlayerCharacter> character);

    /**
     * Add PlayerCharacter to the actual PlayerCharacters unlocked.
     * 
     * @param character : the new PlayerCharacter
     */
    void addCharacter(PlayerCharacter character);

    /**
     * Delete a character from the actual PlayerCharacters unlocked.
     * 
     * @param character : the character to be deleted
     */
    void deleteCharacter(PlayerCharacter character);

    /**
     * Return a list of Strings, containing the toString of pubblic data.
     * 
     * @return a list of string of data
     */
    List<String> getDataList();

}
