package model.user;

import java.util.Optional;
import enumerators.Level;

/**
 * User model. It access to the currentUser singleton instance.
 *
 */
public class UserModelImpl implements UserModel {


    @Override
    public final UserImpl getUser() {
        return CurrentUserImpl.getInstance().getUser();
    }

    @Override
    public final void setUser(final UserImpl user) {
        CurrentUserImpl.getInstance().setUser(user);
    }

    @Override
    public final Optional<Level> getCurrentLevel() {
        return CurrentUserImpl.getInstance().getCurrentLevel();
    }

    @Override
    public final void setCurrentLevel(final Level currentLevel) {
        CurrentUserImpl.getInstance().setCurrentLevel(currentLevel);
    }

    @Override
    public final void setNextCurrentLevel() {
        CurrentUserImpl.getInstance().setNextCurrentLevel();
    }

}
