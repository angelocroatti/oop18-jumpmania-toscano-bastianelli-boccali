package model.user;

import java.util.Optional;
import java.util.Set;

import javax.crypto.Cipher;

import common.CommonStrings;
import utils.CryptDecryptString;
import utils.FileUserManager;

/**
 * User login model.
 */
public class LoginModel {

    private final UserProfile profile;
    private Optional<String> errorMsg;

    /**
     * Login model constructor. The password will be crypted.
     * @param userName : the user name
     * @param password : the user password
     */
    public LoginModel(final String userName, final String password) {
        final String key = CommonStrings.KEY;
        final String pass = CryptDecryptString.execCryptDecrypt(Cipher.ENCRYPT_MODE, key, password);
        this.profile = new UserProfileImpl(userName, pass);
    }

    /**
     * Log the user if username and password are correcter.
     * @return if user is logged in.
     */
    public boolean loginAndSetUser() {
        errorMsg = Optional.empty();
        final Set<UserImpl> s = FileUserManager.load();
        if (!s.isEmpty()) {
            final Optional<UserImpl> user = FileUserManager.searchUserProfile(s, profile);
            if (user.isPresent()) {
                CurrentUserImpl.getInstance().setUser(user.get());
                return true;
            } else {
                this.errorMsg = Optional.of("Wrong username and/or password");
            }
        } else {
            this.errorMsg = Optional.of("Create a profile before login!");
        }
        return false;
    }

    /**
     * Register the user if username isn't already present in the file, then
     * set it as current user.
     * @return true if logged in, false otherwise
     */
    public boolean registerAndSetUser() {
        errorMsg = Optional.empty();
        final Set<UserImpl> s = FileUserManager.load();

        if (s.isEmpty() || !FileUserManager.searchUsername(s, profile.getUsername()).isPresent()) {
            CurrentUserImpl.getInstance().setUser(new UserImpl(profile));
            FileUserManager.update(CurrentUserImpl.getInstance().getUser());
            return true;
        } else if (FileUserManager.searchUsername(s, profile.getUsername()).isPresent()) {
            this.errorMsg = Optional.of("Profile already exist!");
        }
        return false;
    }

    /**
     *Return an optional representing the last error message.
     *Optional.empty if log correctly.
     *@return  last error message
     */
    public Optional<String> getErrorMsg() {
        return errorMsg;
    }
}
