package view.menu;

import java.util.ArrayList;
import java.util.List;

import common.MsgStrings;
import controller.menu.Controller;
import javafx.scene.control.Button;

/**
 * Create a concrete win view. Extends a RowsView.
 */
public class WinView extends RowsView {

    private Button nextLevel;

    /**
     * Win menu.
     * @param c : win controller
     */
    public WinView(final Controller c) {
        super(c);
    }

    @Override
    public final String setTitle() {
        return MsgStrings.WIN;
    }

    @Override
    public final List<Button> setButtons() {
        nextLevel = new MsgEventButton(this, MsgStrings.NEXT_LEVEL).getButton();

        final List<Button> list = new ArrayList<Button>();
        list.add(new MsgEventButton(this, MsgStrings.RESUME).getButton());
        list.add(nextLevel);
        list.add(new MsgEventButton(this, MsgStrings.MENU).getButton());
        list.add(new AudioEventButton(this).getButton());
        return list;
    }

    /**
     * Disable next level button.
     */
    public void disableNextLevel() {
        this.nextLevel.setDisable(true);
    }

}
