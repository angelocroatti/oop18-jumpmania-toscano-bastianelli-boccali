package view.menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import common.CommonStrings;
import common.MsgStrings;
import controller.menu.Controller;
import enumerators.PlayerCharacter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import view.ScoreView;

/**
 * Create a concrete shop view. Extends a GenericView.
 */
public class ShopView extends ScoreView {

    private static final Double TOP = CommonStrings.WINDOW_WIDTH / 100.0 * 10;
    private static final Double BOTTOM = CommonStrings.WINDOW_WIDTH / 100.0 * 20;
    private static final Double LATERAL = 0.0;

    private PlayerCharacter characterSelected;
    private final List<CharacterShopBorderPane> charactersList;

    /**
     * MainMenuView constructor.
     * 
     * @param c : MainManu controller
     */
    public ShopView(final Controller c) {
        super(c);
        final BorderPane box = new BorderPane();
        super.init(box, TOP, BOTTOM, LATERAL, LATERAL);

        // top
        final Label title = new Label(MsgStrings.SHOP);
        title.setId("title");
        final VBox titleBox = new VBox();
        titleBox.getChildren().add(title);
        titleBox.getChildren().add(super.getHBox());

        // center
        final TilePane charactersBox = new TilePane();
        charactersList = new ArrayList<>();
        Arrays.asList(PlayerCharacter.values()).forEach(player -> {
            charactersList.add(new CharacterShopBorderPane(player, this) {
                @Override
                public void selectCharacter() {
                    setCharacterSelected(player);
                }
            });
        });
        charactersBox.getChildren().addAll(charactersList);

        // buttom
        final Button menuBtn = new MsgEventButton(this, MsgStrings.MENU).getButton();
        final HBox buttonBox = new HBox();
        buttonBox.getChildren().add(menuBtn);

        box.setTop(titleBox);
        box.setCenter(charactersBox);
        box.setBottom(buttonBox);
    }

    /**
     * Set buttons enable or disable foreach player character. It must be call after
     * creation to set button currectly.
     * 
     * @param charactersSet : set of unlocked character
     * @param current       : the current character selected
     * @param coin          : actual coin owned
     */
    public void updateActivation(final Set<PlayerCharacter> charactersSet, final PlayerCharacter current,
            final int coin) {
        for (final CharacterShopBorderPane characterShop : charactersList) {
            characterShop.setButtonActivation(charactersSet, current, coin);
        }
        this.characterSelected = null;
    }

    /**
     * Set character to buy.
     * 
     * @param characterSelected from view
     */
    private void setCharacterSelected(final PlayerCharacter characterSelected) {
        this.characterSelected = characterSelected;
    }

    /**
     * Get character to buy from the shop.
     * 
     * @return selected player
     */
    public PlayerCharacter getSelectedPlayer() {
        return this.characterSelected;
    }
}
