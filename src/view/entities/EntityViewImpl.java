package view.entities;

import java.util.Optional;

import org.jbox2d.common.Vec2;

import enumerators.SpecificType;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.CacheHint;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.entities.EntityModel;
import utils.Box2DUtils;
import view.GameView;

/**
 * Implementation of the entity view.
 */
public class EntityViewImpl implements EntityView {

    private static final String IMAGE_PATH = "images/";

    private final EntityModel entityModel;
    private final ImageView image;
    private final Dimension2D dimension;
    private final GameView gView;
    private Point2D position;

    /**
     * Create an entity view implementation.
     * @param entityModel the model of the entity
     * @param type the specific type of the entity
     * @param gView the game view
     */
    public EntityViewImpl(final EntityModel entityModel, final SpecificType type, final GameView gView) {
        this.entityModel = entityModel;
        this.gView = gView;
        this.dimension = Box2DUtils.vecToDim(entityModel.getDimension());
        final Optional<Image> tmpImage = Optional.ofNullable(new Image(IMAGE_PATH + type.getImageName()));
        if (tmpImage.isPresent()) {
            this.image = new ImageView(tmpImage.get());
            initView();
        } else {
            throw new IllegalArgumentException("Image not found for type" + type.toString());
        }
    }

    @Override
    public final ImageView getImage() {
        return this.image;
    }

    private void initView() {
        position = Box2DUtils.vecToPoint(entityModel.getPhysicPosition());
        image.setCache(true);
        image.setCacheHint(CacheHint.SPEED);
        image.setX(position.getX());
        image.setY(position.getY());
        gView.getViewNodes().add(image);
    }

    @Override
    public final void updateView() {
        position = gView.getCamera().viewPointToCamera(Box2DUtils.vecToPoint(entityModel.getPhysicPosition()));
        image.setX(position.getX());
        image.setY(position.getY());
    }

    @Override
    public final void remove() {
        gView.getViewNodes().remove(image);
    }

    @Override
    public final void show() {
        gView.getViewNodes().add(image);
    }

    @Override
    public final Vec2 getViewPosition() {
        return new Vec2((float) image.getX(), (float) image.getY());
    }

    @Override
    public final void fitViewToDimension(final boolean preserveRatio) {
        image.setPreserveRatio(preserveRatio);
        image.setFitWidth(dimension.getWidth());
        image.setFitHeight(dimension.getHeight());
    }

}
