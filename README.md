# JumpMania

# Come giocare:

Per giocare è necessario prima di tutto registrarsi. In seguito si potrà fare login utilizzando le credenziali registrate.

Il personaggio può muoversi solo a sinistra e destra, rispettivamente coi tasti A e D. La pressione continuata del tasto aumenta l'intensità del movimento.